<?php

namespace tests;


use Symfony\Component\Config\Definition\Exception\Exception;
use Template\Gateways\Datasources\MemoryDatasource;
use Template\Exceptions\DocumentAlreadyExistsDatasourceException;

class MemoryDatasourceTest extends \PHPUnit_Framework_TestCase {

    protected $_datasource;
    protected $_fixtures = array(
        array('id' => 'test_id0', 'value' => 'test_value0'),
        array('id' => 'test_id1', 'value' => 'test_value1'),
        array('id' => 'test_id2', 'value' => 'test_value2'),
        array('id' => 'test_id3', 'value' => 'test_value3')
    );

    public function testCreateMethodValidScenario() {
        $this->assertEquals(
            array('id' => 'test_id4', 'value' => 'test_value4'),
            $this->getDatasource()->create(array('id' => 'test_id4', 'value' => 'test_value4'))
        );
    }

    public function testCreateDocumentWithAlreadyExistedId() {
        $throwed = false;

        try {
            $this->getDatasource()->create(array('id' => 'test_id3', 'value' => 'test_value3'));
        } catch (DocumentAlreadyExistsDatasourceException $e) {
            $throwed = true;
        }

        $this->assertTrue($throwed);
    }

    public function testReadMethodWithExistedDocument() {
        $this->assertEquals(
            array('id' => 'test_id0', 'value' => 'test_value0'),
            $this->getDatasource()->read("test_id0")
        );
    }

    public function testReadMethodWithNonExistedDocument() {
        $this->assertNull(
            $this->getDatasource()->read("test_id4")
        );
    }

    protected function setUp() {
        $this->_datasource = new MemoryDatasource();
        $this->_init_fixtures();
    }

    /**
     * @return array
     */
    public function getFixtures() {
        return $this->_fixtures;
    }

    /**
     * @return MemoryDatasource
     */
    public function getDatasource() {
        return $this->_datasource;
    }

    protected function _init_fixtures() {

        foreach ($this->getFixtures() as $fixture) {
            $this->getDatasource()->create($fixture);
        }
    }
}