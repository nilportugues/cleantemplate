<?php

namespace tests;


use Template\Gateways\Datasources\MemoryDatasource;
use Template\Gateways\UserGateway;
use Template\Validators\Rules\KeyInArrayAndNotEmptyValidationRule;
use Template\Validators\Rules\UserCredentialsValidationRule;
use Template\Validators\ValidationRuleList;

class ValidatorsTest extends \PHPUnit_Framework_TestCase {

    public function testValidKeyInArrayAndNotEmptyValidationRule() {
        $inputArray = array(
            'test0' => 'test_value0',
            'test1' => 'test_value1',
            'test2' => 'test_value2',
        );

        $validationRule = new KeyInArrayAndNotEmptyValidationRule($inputArray, true, "Key not exists.");

        $validationRuleResult = $validationRule->apply('test0', 'test_context');
        $this->assertFalse($validationRuleResult->hasErrors());
    }

    public function testInValidKeyInArrayAndNotEmptyValidationRule() {
        $inputArray = array(
            'test0' => 'test_value0',
            'test1' => 'test_value1',
            'test2' => 'test_value2',
        );

        $validationRule = new KeyInArrayAndNotEmptyValidationRule($inputArray, true, "Key not exists.");

        $validationRuleResult = $validationRule->apply('test', 'test_context');

        $this->assertTrue($validationRuleResult->hasErrors());
        $this->assertEquals(array(array("Key not exists." => 'test_context')), $validationRuleResult->getErrors());
    }

    public function testValidUserCredentialsValidationRule() {
        $memoryDatasource = new MemoryDatasource();

        $input = array(
            'username' => 'ramon',
            'password' => 'password'
        );

        $memoryDatasource->create($input);

        $gateway = new UserGateway($memoryDatasource);

        $validationRule = new UserCredentialsValidationRule($gateway, true, "Invalid credentials were given.");

        $validationRuleResult = $validationRule->apply($input, 'user_context');

        $this->assertFalse($validationRuleResult->hasErrors());
    }

    public function testInValidUserCredentialsValidationRule() {
        $memoryDatasource = new MemoryDatasource();

        $input = array(
            'username' => 'ramon',
            'password' => 'invalid_password'
        );

        $memoryDatasource->create(array(
            'username' => 'ramon',
            'password' => 'password'
        ));

        $gateway = new UserGateway($memoryDatasource);

        $validationRule = new UserCredentialsValidationRule($gateway, true, "Invalid credentials were given.");

        $validationRuleResult = $validationRule->apply($input, 'user_context');

        $this->assertTrue($validationRuleResult->hasErrors());
        $this->assertEquals(array(array("Invalid credentials were given." => 'user_context')), $validationRuleResult->getErrors());

        $validationRuleResult = $validationRule->apply(array(), 'user_context');
        $this->assertTrue($validationRuleResult->hasErrors());
        $this->assertEquals(array(array("Invalid credentials were given." => 'user_context')), $validationRuleResult->getErrors());
    }

    public function testValidationRuleList() {
        $validationRuleList = new ValidationRuleList();
        $inputArray = array(
            'test0' => 'test_value0',
            'test1' => 'test_value1',
            'test2' => 'test_value2',
        );

        $validationRuleList->addRules(
            array(
                array('rule' => new KeyInArrayAndNotEmptyValidationRule($inputArray, true, "Key not exists."),
                      'value' => 'test0',
                      'context' => 'test'),

                array('rule' => new KeyInArrayAndNotEmptyValidationRule($inputArray, true, "Key not exists."),
                    'value' => 'test1',
                    'context' => 'test'),

                array('rule' => new KeyInArrayAndNotEmptyValidationRule($inputArray, true, "Key not exists."),
                    'value' => 'test2',
                    'context' => 'test'),
            )
        );

        $validationRuleListResult = $validationRuleList->apply();

        $this->assertFalse($validationRuleListResult->hasErrors());
    }

}