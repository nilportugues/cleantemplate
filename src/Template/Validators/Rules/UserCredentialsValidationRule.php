<?php

namespace Template\Validators\Rules;


use Template\Core\AbstractGateway;
use Template\Core\AbstractValidationRule;
use Template\Entities\UserEntity;
use Template\Validators\ValidationRuleList;

class UserCredentialsValidationRule extends AbstractValidationRule {

    protected $_gateway;

    /**
     * @param AbstractGateway $gateway
     * @param bool $critical
     * @param string $message
     */
    function __construct(AbstractGateway $gateway, $critical=true, $message="") {
        $this->_gateway = $gateway;
        parent::__construct($critical, $message);
    }

    /**
     * @param mixed $value
     * @return bool
     */
    protected function _apply($value) {
        return $this->_validateInput($value) && $this->_validateCredentials($value);
    }

    /**
     * @param array $value
     * @return bool
     */
    protected function _validateCredentials(Array $value) {
        $user = $this->_gateway->read($value['username'], 'username');

        if ($user instanceof UserEntity) {
            return $user->password == $value['password'];
        }

        return false;
    }

    /**
     * @param array $value
     * @return bool
     */
    protected function _validateInput(Array $value) {
        $validationRuleList = new ValidationRuleList();
        $validationRuleList->addRules(
            array(
                array('rule' => new KeyInArrayAndNotEmptyValidationRule($value),
                    'value' => 'username'
                ),
                array('rule' => new KeyInArrayAndNotEmptyValidationRule($value),
                    'value' => 'password'
                ),
            )
        );
        $validationRuleListResult = $validationRuleList->apply();

        return !$validationRuleListResult->hasErrors();
    }
}