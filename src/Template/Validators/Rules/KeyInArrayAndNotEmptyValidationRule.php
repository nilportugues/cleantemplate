<?php

namespace Template\Validators\Rules;


use Template\Core\AbstractValidationRule;

class KeyInArrayAndNotEmptyValidationRule extends AbstractValidationRule {

    protected $_array;

    /**
     * @param array $array
     * @param bool $critical
     * @param string $message
     */
    function __construct(Array $array, $critical=true, $message="") {
        $this->_array = $array;
        parent::__construct($critical, $message);
    }

    /**
     * @param mixed $value
     * @return bool
     */
    protected function _apply($value) {
        return array_key_exists($value, $this->_array) && !empty($this->_array[$value]);
    }
}