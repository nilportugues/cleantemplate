<?php

namespace Template\Validators;


class ValidationResult {

    protected $_errors = array();

    /**
     * @param $message
     * @param string $context
     */
    public function addError($message, $context="default") {
        $this->_errors[] = array($message => $context);
    }

    /**
     * @param ValidationResult $result
     */
    public function addResult(ValidationResult $result) {

        foreach ($result->getErrors() as $errorContainer) {

            foreach ($errorContainer as $message => $context) {
                $this->addError($message, $context);
            }
        }
    }

    /**
     * @return array
     */
    public function getErrors() {
        return $this->_errors;
    }

    /**
     * @return bool
     */
    public function hasErrors() {
        return count($this->getErrors()) > 0;
    }
}