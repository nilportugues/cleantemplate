<?php

namespace Template\Gateways;


use Template\Core\AbstractGateway;
use Template\Entities\UserEntity;

class UserGateway extends AbstractGateway {

    /**
     * @return \Template\Core\AbstractEntity
     */
    public function getEntity() {
        return new UserEntity();
    }

}