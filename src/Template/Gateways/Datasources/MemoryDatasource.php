<?php

namespace Template\Gateways\Datasources;


use Template\Core\AbstractDatasource;
use Template\Exceptions\DocumentAlreadyExistsDatasourceException;

class MemoryDatasource extends AbstractDatasource {

    protected $_container = array();

    /**
     * @return string
     */
    public function getPrimaryId() {
        return "id";
    }

    /**
     * @param array $document
     * @return array
     * @throws \Template\Exceptions\DocumentAlreadyExistsDatasourceException
     */
    public function create(Array $document) {
        $document = $this->_generatePrimaryKeyIfNotExists($document);

        if ($this->_documentAlreadyExistsInContainer($document)) {
            throw new DocumentAlreadyExistsDatasourceException("Document already exists.");
        }

        $this->_addDocumentToContainer($document);

        return $document;
    }

    /**
     * @param string $value
     * @param string|null $key
     * @return array|null
     */
    public function read($value, $key=null) {

        if ($this->_isPrimaryKey($key)) {
            return $this->_getDocumentByPrimaryKey($value);
        }

        return $this->_getDocumentByNotPrimaryKey($value, $key);
    }

    /**
     * @codeCoverageIgnore
     * @param array $document
     * @return array
     */
    public function update(Array $document) {
        // TODO: Implement update() method.
    }

    /**
     * @codeCoverageIgnore
     * @param string $value
     * @param string $key
     * @return boolean
     */
    public function delete($value, $key) {
        // TODO: Implement delete() method.
    }

    /**
     * @codeCoverageIgnore
     * @param array $conditions
     * @return array
     */
    public function readAll(Array $conditions = array()) {
        // TODO: Implement readAll() method.
    }

    /**
     * @codeCoverageIgnore
     * @return boolean
     */
    public function drop() {
        // TODO: Implement drop() method.
    }

    /**
     * @codeCoverageIgnore
     * @return boolean
     */
    public function truncate() {
        // TODO: Implement truncate() method.
    }

    /**
     * @param array $document
     * @return array
     */
    protected function _generatePrimaryKeyIfNotExists(Array $document) {

        if (!array_key_exists($this->getPrimaryId(), $document)
            || empty($document[$this->getPrimaryId()]))
        {
            $document[$this->getPrimaryId()] = uniqid($this->getPrimaryId(), true);
        }

        return $document;
    }

    /**
     * @param array $document
     */
    protected function _addDocumentToContainer(Array $document) {
        $this->_container[$document[$this->getPrimaryId()]] = $document;
    }

    /**
     * @param array $document
     * @return bool
     */
    private function _documentAlreadyExistsInContainer(Array $document) {
        return array_key_exists($document[$this->getPrimaryId()], $this->_container);
    }

    /**
     * @param string $key
     * @return bool
     */
    protected function _isPrimaryKey($key) {
        return $key == $this->getPrimaryId() || $key == null;
    }

    /**
     * @param string $value
     * @return array|null
     */
    protected function _getDocumentByPrimaryKey($value) {
        return (array_key_exists($value, $this->_container)) ? $this->_container[$value] : null;
    }

    /**
     * @param string $value
     * @param string $key
     * @return array|null
     */
    protected function _getDocumentByNotPrimaryKey($value, $key) {

        foreach ($this->_container as $document) {
            if (array_key_exists($key, $document) && $document[$key] == $value) {
                return $document;
            }
        }

        return null;
    }
}