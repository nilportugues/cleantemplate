<?php

namespace Template\Core;


abstract class AbstractUsecase {
    abstract public function execute();
}
