<?php

namespace Template\Core;


use Template\Validators\ValidationResult;

abstract class AbstractValidationRule {

    function __construct($critical=true, $message='') {
        $this->_message = (empty($message)) ? $this->getDefaultMessage() : $message;
        $this->_critical = $critical;
    }

    /**
     * @param mixed $value
     * @param string $context
     * @return ValidationResult
     */
    public function apply($value, $context='default') {
        $validationResult = new ValidationResult();

        if (!$this->_apply($value)) {
            $validationResult->addError($this->getMessage(), $context);
        }

        return $validationResult;
    }

    /**
     * @return string
     */
    public function getMessage() {
        return $this->_message;
    }

    /**
     * @return string
     */
    public function getDefaultMessage() {
        return ""; // TODO: Implement default Message method.
    }

    /**
     * @return boolean
     */
    public function isCritical() {
        return $this->_critical;
    }

    /**
     * @param mixed $value
     * @return boolean
     */
    abstract protected function _apply($value);
}