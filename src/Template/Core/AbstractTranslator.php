<?php

namespace Template\Core;


abstract class AbstractTranslator {
    abstract public function trans($string, $params=array());
} 