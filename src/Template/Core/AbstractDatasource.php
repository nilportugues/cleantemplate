<?php

namespace Template\Core;


abstract class AbstractDatasource {

    /**
     * @param array $document
     * @return array
     */
    abstract public function create(Array $document);

    /**
     * @param string $value
     * @param string|null $key
     * @return array
     */
    abstract public function read($value, $key=null);

    /**
     * @param array $document
     * @return array
     */
    abstract public function update(Array $document);

    /**
     * @param string $value
     * @param string $key
     * @return boolean
     */
    abstract public function delete($value, $key);

    /**
     * @param array $conditions
     * @return array
     */
    abstract public function readAll(Array $conditions=array());

    /**
     * @return boolean
     */
    abstract public function drop();

    /**
     * @return boolean
     */
    abstract public function truncate();

    /**
     * @return string
     */
    abstract public function getPrimaryId();
}