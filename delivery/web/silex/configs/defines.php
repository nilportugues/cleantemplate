<?php
define('__ROOT_DIR__', __DIR__ . '/..');
define('__RUNTIME_DIR__', __ROOT_DIR__ . '/runtime');
define('__VENDOR_DIR__', __ROOT_DIR__ . '/vendor');
define('__TEMPLATES_DIR__', __ROOT_DIR__ . '/templates');
define('__LOCALES_DIR__', __ROOT_DIR__ . '/locales');
