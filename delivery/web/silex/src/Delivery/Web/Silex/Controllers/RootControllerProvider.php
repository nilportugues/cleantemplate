<?php

namespace Delivery\Web\Silex\Controllers;

use Delivery\Web\Silex\Controllers\Actions\ShowHomePageControllerAction;
use Delivery\Web\Silex\Controllers\Actions\ProcessUserLoginControllerAction;
use Silex\Application;
use Silex\ControllerCollection;
use Silex\ControllerProviderInterface;
use Delivery\Web\Silex\Controllers\Actions\ShowLoginPageControllerAction;

class RootControllerProvider implements ControllerProviderInterface {

    /**
     * @param Application $app
     * @return ControllerCollection
     */
    public function connect(Application $app) {
        /* @var $controllers ControllerCollection */
        $controllers = $app['controllers_factory'];

        $controllers->get('/', array(new ShowHomePageControllerAction(), 'execute'));
        $controllers->get('/login', array(new ShowLoginPageControllerAction(), 'execute'));
        $controllers->post('/login', array(new ProcessUserLoginControllerAction(), 'execute'));

        return $controllers;
    }
}
