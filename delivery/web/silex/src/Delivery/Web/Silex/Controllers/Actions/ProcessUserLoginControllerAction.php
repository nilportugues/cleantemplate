<?php

namespace Delivery\Web\Silex\Controllers\Actions;


use Silex\Translator as SilexTranslator;
use Delivery\Web\Silex\Adapters\Translator;
use Delivery\Web\Silex\Core\AbstractControllerAction;
use Delivery\Web\Silex\Adapters\Application;
use Delivery\Web\Silex\Outputs\LoginPageOutput;
use Symfony\Component\HttpFoundation\Request;
use Template\Gateways\Datasources\MemoryDatasource;
use Template\Gateways\UserGateway;
use Template\Usecases\UserLoginUseCase;
use Twig_Environment;

class ProcessUserLoginControllerAction extends AbstractControllerAction {

    /**
     * @param Application $app
     * @return mixed
     */
    public function execute(Application $app) {

        /** @var Request $request */
        $request = $app['request'];
        $app['locale'] = $request->query->get('_locale', 'en');

        /** @var Twig_Environment $twig */
        $twig = $app['twig'];

        /** @var SilexTranslator $silexTranslator */
        $silexTranslator = $app['translator'];

        $parameters = $request->request->all();

        /** test datasource  */
        $datasource = new MemoryDatasource();
        $datasource->create(array('username' => 'test',
                                  'password' => 'test123'));
        /** end */

        $gateway = new UserGateway($datasource);
        $output = new LoginPageOutput($twig, $request);
        $translator = new Translator($silexTranslator);

        $usecase = new UserLoginUseCase($parameters, $gateway, $output, $translator);

        return $usecase->execute();
    }
}
