<?php

namespace tests;


use Silex\WebTestCase;
use Symfony\Component\HttpKernel\HttpKernel;

class LoginPageTest extends WebTestCase {

    /**
     * Creates the application.
     *
     * @return HttpKernel
     */
    public function createApplication() {
        $app = require(__DIR__ . '/../bootstrap.php');
        require(__DIR__ . '/../routes.php');

        return $app;
    }

    public function testSuccessfulLogin() {
        $client = $this->createClient();
        $crawler = $client->request('GET', '/login');

        $form = $crawler->selectButton('Submit!')->form(array(
            'username' => 'test',
            'password' => 'test123'
        ));
        $crawler = $client->submit($form, array());
        $this->assertEquals(0, $crawler->filter('ul')->count());
    }

    public function testInvalidUsername() {
        $client = $this->createClient();
        $crawler = $client->request('GET', '/login');

        $form = $crawler->selectButton('Submit!')->form(array());
        $crawler = $client->submit($form, array(
            'username' => '',
            'password' => 'test123'
        ));
        $this->assertEquals(1, $crawler->filter('li')->count());
        $this->assertRegExp('#Username should be specified#', $crawler->filter('li')->text());
    }

    public function testInvalidPassword() {
        $client = $this->createClient();
        $crawler = $client->request('GET', '/login');

        $form = $crawler->selectButton('Submit!')->form(array());
        $crawler = $client->submit($form, array(
            'username' => 'test',
            'password' => ''
        ));
        $this->assertEquals(1, $crawler->filter('li')->count());
        $this->assertRegExp('#Password should be specified#', $crawler->filter('li')->text());
    }

    public function testInvalidCredentials() {
        $client = $this->createClient();
        $crawler = $client->request('GET', '/login');

        $form = $crawler->selectButton('Submit!')->form(array());
        $crawler = $client->submit($form, array(
            'username' => 'test',
            'password' => 'invalid_test'
        ));
        $this->assertEquals(1, $crawler->filter('li')->count());
        $this->assertRegExp('#Invalid credentials were given#', $crawler->filter('li')->text());
    }

    public function testValidCredentials() {
        $client = $this->createClient();
        $crawler = $client->request('GET', '/login');

        $form = $crawler->selectButton('Submit!')->form(array());
        $crawler = $client->submit($form, array(
            'username' => 'test',
            'password' => 'test123'
        ));

        $this->assertRegExp('#Welcome to secure page#', $crawler->filter('body')->text());
    }

    public function testTranslationsLabelUsername() {
        $client = $this->createClient();
        $crawler = $client->request('GET', '/login?_locale=es');

        $this->assertRegExp('#Nombre de usuario#', $crawler->filter('label[for=username]')->text());
    }

    public function testTranslationsLabelPassword() {
        $client = $this->createClient();
        $crawler = $client->request('GET', '/login?_locale=es');

        $this->assertRegExp('#Contraseña#', $crawler->filter('label[for=password]')->text());
    }

    public function testTranslationsSuccessLogin() {
        $client = $this->createClient();
        $crawler = $client->request('GET', '/login?_locale=es');

        $form = $crawler->selectButton('Presentar!')->form(array());
        $crawler = $client->submit($form, array(
            'username' => 'test',
            'password' => 'test123'
        ));

        $this->assertRegExp('#Bienvenido a la página asegurar#', $crawler->filter('body')->text());
    }
}