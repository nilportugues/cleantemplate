<?php

namespace tests;


use Silex\WebTestCase;
use Symfony\Component\HttpKernel\HttpKernel;

class HttpCodesTest extends WebTestCase {

    /**
     * Creates the application.
     *
     * @return HttpKernel
     */
    public function createApplication() {
        $app = require(__DIR__ . '/../bootstrap.php');
        require(__DIR__ . '/../routes.php');

        return $app;
    }

    public function test404() {
        $client = $this->createClient();
        $client->request('GET', '/give-me-a-404');
        $this->assertEquals(404, $client->getResponse()->getStatusCode());
    }

    public function test302() {
        $client = $this->createClient();
        $client->request('GET', '/');
        $this->assertEquals(302, $client->getResponse()->getStatusCode());
    }

    public function test200() {
        $client = $this->createClient();
        $client->request('GET', '/login');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }
}