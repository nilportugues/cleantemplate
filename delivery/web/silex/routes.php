<?php

use Symfony\Component\HttpFoundation\Response;
use Delivery\Web\Silex\Controllers\RootControllerProvider;

$app->mount('/', new RootControllerProvider());

$app->error(function(Exception $e, $code) use ($app) {
    $response = new Response($app['twig']->render('errors/500.twig'), 500);

    if($code == 404) {
        $response = new Response($app['twig']->render('errors/404.twig'), 404);
    }

    return $response;
});