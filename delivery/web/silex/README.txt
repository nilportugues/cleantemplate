To run phpunit with coverage report (XDebug needed):
>> bin/phpunit --coverage-html ./report

To run dev-http server:
>> cd public ; php -S localhost:8000

