To install core component:
>> cd CleanTemplate; php bin/composer.phar install

To install delivery component:
>> cd CleanTemplate/delivery/web/silex; php bin/composer.phar install

To run phpunit with coverage report (XDebug needed):
>> bin/phpunit --coverage-html ./report

To run dev-http server:
>> cd delivery/web/silex/public ; php -S localhost:8000

